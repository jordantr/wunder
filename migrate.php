<?php

/**
 *
 * This file may not be redistributed in whole or significant part
 *
 * ------------------- THIS IS NOT FREE SOFTWARE -------------------
 *
 * Copyright 2021 All Rights Reserved
 *
 * Migrate - Migrations file of the MVC
 *
 * @file        index.php
 * @category    migrate
 * @author      Jordan Trifonov
 *
 */

// Define root path to load the files later
define('ROOT_PATH', str_replace("\\", "/", dirname(__FILE__)) . "/");

// Load config and language files
require_once ROOT_PATH . "application/config.php";

// Load main framework classes
require_once ROOT_PATH . "framework/core/Registry.class.php";
require_once ROOT_PATH . "framework/core/Database.class.php";
require_once ROOT_PATH . "framework/core/System.class.php";

// Create DB instance
$config_database = $config + $config_db;
$db = new Database($config_database);

########## Countries ##########
$db->query("CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(32) NOT NULL,
  `last_name` varchar(32) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `street` varchar(32) NOT NULL,
  `number` varchar(6) NOT NULL, # Because of different numbers in different countries like 212A for example
  `zip` varchar(12) NOT NULL, # Because of different codes in different countries (not int only)
  `city` varchar(32) NOT NULL,
  `account_owner` varchar(32) NOT NULL,
  `iban` varchar(32) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");