<?php
/*%%SmartyHeaderCode:13810148846023afd99c14c6_12766631%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '099609c01cee33385b9b388c64ffe8853e4717c3' => 
    array (
      0 => 'C:\\xampp\\htdocs\\dev.wunder\\application\\views\\index.tpl.html',
      1 => 1612903430,
      2 => 'file',
    ),
    '681a00f80880db8fe7516e5b01f8e8ac04214cb1' => 
    array (
      0 => 'C:\\xampp\\htdocs\\dev.wunder\\application\\views\\_messages.tpl.html',
      1 => 1612903420,
      2 => 'file',
    ),
    'e601bae2d80c71f65597fbf2a29f27aabaa8ad10' => 
    array (
      0 => 'C:\\xampp\\htdocs\\dev.wunder\\application\\views\\_step_1.tpl.html',
      1 => 1612906049,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13810148846023afd99c14c6_12766631',
  'tpl_function' => 
  array (
  ),
  'variables' => 
  array (
    'step' => 0,
    'config' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_6023afd9aa8ea0_53116609',
  'cache_lifetime' => 3600,
),true);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_6023afd9aa8ea0_53116609')) {
function content_6023afd9aa8ea0_53116609 ($_smarty_tpl) {
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Jordan Trifonov">
    <title>Signup Form - Step 1/3</title>

    <!-- Bootstrap core CSS -->
    <link href="http://dev.wunder/htdocs/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <!-- Favicons -->
    <meta name="theme-color" content="#7952b3">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
  </head>

  <body class="bg-light">
    
    <div class="container">
      <main>
        <div class="py-5 text-center">
          <img class="d-block mx-auto mb-4" src="http://dev.wunder/htdocs/images/logo.jpg" alt="" width="200">
          <h2>Signup form</h2>
          <p class="lead">Step 1/3</p>
        </div>

        <div class="row g-3">
          
          <div class="col-md-7 col-lg-2"></div>
          
          <div class="col-md-7 col-lg-8">
            <form method="post" action="" id="signup_form">

              <div class="alert alert-success" role="alert" id="message_success" style="display: none"></div>
<div class="alert alert-danger" role="alert" id="message_error" style="display: none"></div>

              <input type="hidden" name="step" value="1">

              <div class="row g-3 ">
  <div class="col-sm-12">
    <label for="first_name" class="form-label">First name</label>
    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Your first name" value="">
  </div>
</div>

<div class="row g-3 mt-1">
  <div class="col-sm-12">
    <label for="last_name" class="form-label">Last name</label>
    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Your last name" value="">
  </div>
</div>

<div class="row g-3 mt-1 mb-5">
  <div class="col-12">
    <label for="phone" class="form-label">Phone</label>
    <input type="text" class="form-control" id="phone" name="phone" placeholder="Your Phone" value="">
  </div>
</div>

              <button class="w-100 btn btn-primary btn-lg" type="button" id="submit_button">Continue &raquo;</button>

            </form>
          </div>
          
        </div>
      </main>

      <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; 2021 Jordan Trifonov</p>
      </footer>
    </div>

    <script src="http://dev.wunder/htdocs/js/jquery-3.5.1.min.js"></script>
    <script src="http://dev.wunder/htdocs/js/bootstrap.bundle.min.js"></script>
    <script src="http://dev.wunder/htdocs/js/main.js"></script>

  </body>
</html><?php }
}
?>