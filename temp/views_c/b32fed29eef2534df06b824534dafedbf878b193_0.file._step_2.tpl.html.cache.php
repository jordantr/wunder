<?php /* Smarty version 3.1.27, created on 2021-02-10 12:04:53
         compiled from "C:\xampp\htdocs\dev.wunder\application\views\_step_2.tpl.html" */ ?>
<?php
/*%%SmartyHeaderCode:13819123466023afc5403991_58301427%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b32fed29eef2534df06b824534dafedbf878b193' => 
    array (
      0 => 'C:\\xampp\\htdocs\\dev.wunder\\application\\views\\_step_2.tpl.html',
      1 => 1612906086,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13819123466023afc5403991_58301427',
  'variables' => 
  array (
    'user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_6023afc54087d1_68310418',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_6023afc54087d1_68310418')) {
function content_6023afc54087d1_68310418 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '13819123466023afc5403991_58301427';
?>
<div class="row g-3">
  <div class="col-sm-12">
    <label for="street" class="form-label">Street</label>
    <input type="text" class="form-control" id="street" name="street" placeholder="Your street" value="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['user']->value['street'])===null||$tmp==='' ? '' : $tmp);?>
">
  </div>
</div>

  
<div class="row g-3 mt-1">
  <div class="col-sm-12">
    <label for="number" class="form-label">House number</label>
    <input type="text" class="form-control" id="number" name="number" placeholder="Your house number" value="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['user']->value['number'])===null||$tmp==='' ? '' : $tmp);?>
">
  </div>
</div>

<div class="row g-3 mt-1">
  <div class="col-sm-12">
    <label for="zip" class="form-label">Zip</label>
    <input type="text" class="form-control" id="zip" name="zip" placeholder="Your ZIP code" value="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['user']->value['zip'])===null||$tmp==='' ? '' : $tmp);?>
">
  </div>
</div>

<div class="row g-3 mt-1 mb-5">
  <div class="col-sm-12">
    <label for="city" class="form-label">City</label>
    <input type="text" class="form-control" id="city" name="city" placeholder="Your city name" value="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['user']->value['city'])===null||$tmp==='' ? '' : $tmp);?>
">
  </div>
</div><?php }
}
?>