<?php /* Smarty version 3.1.27, created on 2021-02-10 12:05:13
         compiled from "C:\xampp\htdocs\dev.wunder\application\views\index.tpl.html" */ ?>
<?php
/*%%SmartyHeaderCode:13810148846023afd99c14c6_12766631%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '099609c01cee33385b9b388c64ffe8853e4717c3' => 
    array (
      0 => 'C:\\xampp\\htdocs\\dev.wunder\\application\\views\\index.tpl.html',
      1 => 1612903430,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13810148846023afd99c14c6_12766631',
  'variables' => 
  array (
    'step' => 0,
    'config' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_6023afd99e1491_12946007',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_6023afd99e1491_12946007')) {
function content_6023afd99e1491_12946007 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '13810148846023afd99c14c6_12766631';
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Jordan Trifonov">
    <title>Signup Form - Step <?php echo $_smarty_tpl->tpl_vars['step']->value;?>
/3</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $_smarty_tpl->tpl_vars['config']->value['site_css'];?>
bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <!-- Favicons -->
    <meta name="theme-color" content="#7952b3">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
  </head>

  <body class="bg-light">
    
    <div class="container">
      <main>
        <div class="py-5 text-center">
          <img class="d-block mx-auto mb-4" src="<?php echo $_smarty_tpl->tpl_vars['config']->value['site_img'];?>
logo.jpg" alt="" width="200">
          <h2>Signup form</h2>
          <p class="lead">Step <?php echo $_smarty_tpl->tpl_vars['step']->value;?>
/3</p>
        </div>

        <div class="row g-3">
          
          <div class="col-md-7 col-lg-2"></div>
          
          <div class="col-md-7 col-lg-8">
            <form method="post" action="" id="signup_form">

              <?php echo $_smarty_tpl->getSubTemplate ("_messages.tpl.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0);
?>


              <input type="hidden" name="step" value="<?php echo $_smarty_tpl->tpl_vars['step']->value;?>
">

              <?php echo $_smarty_tpl->getSubTemplate ("_step_".((string)$_smarty_tpl->tpl_vars['step']->value).".tpl.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0);
?>


              <button class="w-100 btn btn-primary btn-lg" type="button" id="submit_button">Continue &raquo;</button>

            </form>
          </div>
          
        </div>
      </main>

      <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; 2021 Jordan Trifonov</p>
      </footer>
    </div>

    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['config']->value['site_js'];?>
jquery-3.5.1.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['config']->value['site_js'];?>
bootstrap.bundle.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['config']->value['site_js'];?>
main.js"><?php echo '</script'; ?>
>

  </body>
</html><?php }
}
?>