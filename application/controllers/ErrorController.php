<?php

/**
 *
 * This file may not be redistributed in whole or significant part
 *
 * ------------------- THIS IS NOT FREE SOFTWARE -------------------
 *
 * Copyright 2021 All Rights Reserved
 * 
 * Error controller class - Part of the MVC model
 * 
 * @file         ErrorController.php
 * @category     controllers
 * @author       Jordan Trifonov
 *       
 */
Class ErrorController extends Controller {

    // Configuration
    private $config;

    // Smarty
    private $smarty;

    // Language
    private $lang;

    // If actions is not found
    public function _call() {
        System::redirect($this->config['site_url']);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // Get values from register
        $this->config = Registry::get("config");
        $this->smarty = Registry::get("smarty");
        $this->lang = Registry::get("lang");
    }

    // Index action - display the index page
    public function indexAction() {

        // Assign values and display template
        $this->smarty->display("error.tpl.html");
    }
}