<?php

/**
 *
 * This file may not be redistributed in whole or significant part
 *
 * ------------------- THIS IS NOT FREE SOFTWARE -------------------
 *
 * Copyright 2021 All Rights Reserved
 * 
 * Index controller class - Part of the MVC model
 * 
 * @file         IndexController.php
 * @category     controllers
 * @author       Jordan Trifonov
 *       
 */
Class IndexController extends Controller {

    // Configuration
    private $config;

    // Smarty
    private $smarty;

    // Language
    private $lang;

    // If actions is not found
    public function _call() {
        System::redirect($this->config['site_url']);
    }

    // Initialization method - executed with every controller method
    public function init() {

        // Get values from register
        $this->config = Registry::get("config");
        $this->smarty = Registry::get("smarty");
        $this->lang = Registry::get("lang");

        // Load needed libraries
        Loader::loadClass("Crypt");
        Loader::loadClass("Users");
    }

    // Index action - display the index page
    public function indexAction() {

        // Get the needed step
        $step = intval($this->getParam("step", 1));

        // Limit steps number
        if($step < 1 OR $step > 3){
            $step = 1;
        }

        // User data
        $user = array();

        // If the user is already having record - he is returning user
        $id = (!empty($_COOKIE['user'])) ? $_COOKIE['user'] : "";
        if(!empty($id)){

            // Get the user id
            $iduser = Crypt::decrypt($id, $this->config['encryption']['cipher'], $this->config['encryption']['secret_key'], $this->config['encryption']['secret_iv']);

            // Check where to redirect the user
            $user = Users::getUsers(
                array("id" => $iduser),
                array(),
                array("from" => 0, "count" => 1)
            );

            // If no user is found - go back to first step
            if(empty($user)){
                System::redirect("/");
            }

            // If some of fields are empty and he is not on step 1 - redirect back
            if($step != 1){
                if(empty($user['first_name']) OR empty($user['last_name']) OR empty($user['phone'])){
                    System::redirect("/index/index/step/1");
                }
            }
            
            // If some of fields are empty and he is not on step 2 - redirect back
            if($step != 2){
                if(!empty($user['first_name']) AND !empty($user['last_name']) AND !empty($user['phone'])){
                    if(empty($user['street']) OR empty($user['number']) OR empty($user['zip']) OR empty($user['city'])){
                        System::redirect("/index/index/step/2");
                    }
                }
            }
            
            // If some of fields are empty and he is not on step 3 - redirect back
            if($step != 3){
                if(!empty($user['first_name']) AND !empty($user['last_name']) AND !empty($user['phone']) AND !empty($user['street']) AND !empty($user['number']) AND !empty($user['zip']) AND !empty($user['city'])){
                    if(empty($user['account_owner']) OR empty($user['iban'])){
                        System::redirect("/index/index/step/3");
                    }
                }
            }

            // If all data is present - redirect to finish page
            if(!empty($user['first_name']) AND !empty($user['last_name']) AND !empty($user['phone']) AND !empty($user['street']) AND !empty($user['number']) AND !empty($user['zip']) AND !empty($user['city']) AND !empty($user['account_owner']) AND !empty($user['iban'])){
                System::redirect("/index/finish");
            }

        // If the user didn't start
        } else {

            // If trying to access another step
            if($step > 1){
                System::redirect("/");
            }
        }

        // Assign values and display template
        $this->smarty->assign("step", $step);
        $this->smarty->assign("user", $user);
        $this->smarty->display("index.tpl.html");
    }

    // Save user information
    public function saveAction() {

        // Try to get user ID
        $id = (!empty($_COOKIE['user'])) ? $_COOKIE['user'] : "";

        // If there is no ID
        if(empty($id)){

            // Add user
            $iduser = Users::addUser(
                array(
                    "first_name" => "",
                    "last_name" => "",
                    "phone" => "",
                    "street" => "",
                    "number" => "",
                    "zip" => "",
                    "city" => "",
                    "account_owner" => "",
                    "iban" => "",
                    "payment_id" => "",
                )
            );

            // Get encrypted user ID
            $encrypted = Crypt::encrypt($iduser, $this->config['encryption']['cipher'], $this->config['encryption']['secret_key'], $this->config['encryption']['secret_iv']);

            // Store in cookie
            setcookie("user", $encrypted, time() + 24 * 60 * 60, "/");

        // Get the user data
        } else {
            
            // Get the user id
            $iduser = Crypt::decrypt($id, $this->config['encryption']['cipher'], $this->config['encryption']['secret_key'], $this->config['encryption']['secret_iv']);
            if(empty($iduser)){

                // Assign error message
                exit(json_encode(array("error" => "User not found")));
            }
        }

        // Check on which step we are and save the data
        $step = intval($_POST['step']);

        // If step 1
        if($step == 1){

            // Validate form data
            $form = new Form();
            $rules[] = array(
                "field" => "first_name",
                "value" => $_POST['first_name'],
                "rules" => array(
                    "length" => "2,32",
                ),
                "errors" => array(
                    "length" => "Please, enter first name between 2 and 255 chars",
                ),
            );
            $rules[] = array(
                "field" => "last_name",
                "value" => $_POST['last_name'],
                "rules" => array(
                    "length" => "2,32",
                ),
                "errors" => array(
                    "length" => "Please, enter last name between 2 and 255 chars",
                ),
            );
            $rules[] = array(
                "field" => "phone",
                "value" => $_POST['phone'],
                "rules" => array(
                    "length" => "2,32",
                ),
                "errors" => array(
                    "length" => "Please, enter phone between 2 and 255 chars",
                ),
            );

            // Get user data
            $user_data = array(
                "first_name" => $_POST['first_name'],
                "last_name" => $_POST['last_name'],
                "phone" => $_POST['phone']
            );

        // If step 2
        } else if($step == 2){

            // Validate form data
            $form = new Form();
            $rules[] = array(
                "field" => "street",
                "value" => $_POST['street'],
                "rules" => array(
                    "length" => "2,32",
                ),
                "errors" => array(
                    "length" => "Please, enter street name between 2 and 255 chars",
                ),
            );
            $rules[] = array(
                "field" => "number",
                "value" => $_POST['number'],
                "rules" => array(
                    "length" => "1,32",
                ),
                "errors" => array(
                    "length" => "Please, enter house number between 2 and 255 chars",
                ),
            );
            $rules[] = array(
                "field" => "zip",
                "value" => $_POST['zip'],
                "rules" => array(
                    "length" => "2,32",
                ),
                "errors" => array(
                    "length" => "Please, enter zip between 2 and 255 chars",
                ),
            );
            $rules[] = array(
                "field" => "city",
                "value" => $_POST['city'],
                "rules" => array(
                    "length" => "2,32",
                ),
                "errors" => array(
                    "length" => "Please, enter city between 2 and 255 chars",
                ),
            );

            // Get user data
            $user_data = array(
                "street" => $_POST['street'],
                "number" => $_POST['number'],
                "zip" => $_POST['zip'],
                "city" => $_POST['city']
            );
        
        // If step 3
        } else if($step == 3){

            // Validate form data
            $form = new Form();
            $rules[] = array(
                "field" => "account_owner",
                "value" => $_POST['account_owner'],
                "rules" => array(
                    "length" => "2,32",
                ),
                "errors" => array(
                    "length" => "Please, enter account owner between 2 and 255 chars",
                ),
            );
            $rules[] = array(
                "field" => "iban",
                "value" => $_POST['iban'],
                "rules" => array(
                    "length" => "2,32",
                ),
                "errors" => array(
                    "length" => "Please, enter IBAN between 2 and 255 chars",
                ),
            );

            // Get user data
            $user_data = array(
                "account_owner" => $_POST['account_owner'],
                "iban" => $_POST['iban'],
            );
        }


        // Validate data
        $validate = $form->validate($rules);

        // If the data was good
        if (!$validate) {

            // Get first error
            $error = $form->getFirstError();

            // Assign error message
            exit(json_encode($error));
        }

        // Update user
        Users::updateUser(
            $user_data,
            array("id" => $iduser),
            array("from" => 0, "count" => 1)
        );

        // If on first two steps
        if($step == 1 OR $step == 2){

            // Set next page
            $next_step = $step + 1;
            $redirect_url = "/index/index/step/" . $next_step;

        // If on third step
        } else {

            // Group params to send to data storage
            $data_to_send = array(
                "customerId" => $iduser,
                "iban" => $_POST['iban'],
                "owner" => $_POST['account_owner'],
            );

            // Send data to data storage
            $ch = curl_init($this->config['remote_storage_url']);
            curl_setopt_array($ch, array(
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($data_to_send)
            ));
            $response = curl_exec($ch);
            curl_close($ch);

            // Check for errors
            if($response === false){
                die(curl_error($ch));
            }

            // Get response data
            $response_data = json_decode($response, true);

            // Add to users table
            Users::updateUser(
                array("payment_id" => $response_data['paymentDataId']),
                array("id" => $iduser),
                array("from" => 0, "count" => 1)
            );

            // Return user to success page
            $redirect_url = "/index/finish";
        }

        // Return success message
        $output = json_encode(array("success" => true, "redirect_url" => $redirect_url));
        exit($output);
    }

    // Finished registration
    public function finishAction(){

        // Try to get user ID
        $id = (!empty($_COOKIE['user'])) ? $_COOKIE['user'] : "";

        // If there is no ID
        if(!empty($id)){
            
            // Get the user id
            $iduser = Crypt::decrypt($id, $this->config['encryption']['cipher'], $this->config['encryption']['secret_key'], $this->config['encryption']['secret_iv']);
            if(empty($iduser)){
                System::redirect("/");
            }

            // Check where to redirect the user
            $user = Users::getUsers(
                array("id" => $iduser),
                array(),
                array("from" => 0, "count" => 1)
            );

            // If no user is found - go back to first step
            if(empty($user)){
                System::redirect("/");
            }

            // Assign payment ID
            $this->smarty->assign("payment_id", $user['payment_id']);
        }

        // Remove user cookie
        setcookie("user", "", time() - 24 * 60 * 60, "/");

        // Assign values and display template
        $this->smarty->display("finish.tpl.html");
    }
}