<?php

/**
 * NPS 1.0
 *    
 * This file may not be redistributed in whole or significant part.
 * ------------------- NPS IS NOT FREE SOFTWARE -------------------
 * 
 * Copyright 2020 All Rights Reserved
 * 
 * Configuration
 * 
 * @file        config.php
 * @category	configuration
 * @author		Jordan Trifonov
 *   	
 */

// Start sessions if they are not started yet
if (!session_id())
    session_start();

// Server configuration
ini_set('memory_limit', '1024M');
ini_set('display_errors', 1);
date_default_timezone_set('Europe/Sofia');


// Application index URL
$protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
$site_url = $protocol . "dev.wunder/";

// URLs configuration
$config = array(

    // Dev host
    "dev_url" => $site_url,

    // Site
    "site_url" => $site_url,
    "site_img" => $site_url . "htdocs/images/",
    "site_css" => $site_url . "htdocs/css/",
    "site_js" => $site_url . "htdocs/js/",

    // Encryption key
    "encryption" => array(
        "cipher" => "AES-256-CBC",
        "secret_key" => "wUnDd3rR70",
        "secret_iv" => "t0dA1sAG0oDdAy",
    ),

    // Data storage
    "remote_storage_url" => "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data"
);

// DB config
$config_db = array(
    "host" => "localhost",
    "username" => "root",
    "password" => "",
    "database" => "wunder",
    "charset" => "utf8",
    "debugger" => true
);