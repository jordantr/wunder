<?php

/**
 *
 * This file may not be redistributed in whole or significant part
 *
 * ------------------- THIS IS NOT FREE SOFTWARE -------------------
 *
 * Copyright 2021 All Rights Reserved
 *
 * Users model class - Part of the MVC model
 *
 * @file         Users.php
 * @category     models
 * @author       Jordan Trifonov
 *
 */
class Users {

    // Get data
    public static function getUsers($where, $order, $limit) {

        // Get data from DB
        $data = Database::db()
            ->fields("u.*")
            ->table("users", "u")
            ->where($where)
            ->order($order)
            ->limit($limit)
            ->fetchAll();

        // If returning just one record
        if(!empty($limit['count']) && $limit['count'] == 1){

            // Return row
            return reset($data);

            // If returning all records
        } else {

            // Return rows
            return $data;
        }
    }

    // Add data
    public static function addUser($params) {

        // Insert data into DB
        $id = Database::db()
            ->table("users")
            ->fields($params)
            ->insert();

        // Return id of the inserted row
        return $id;
    }

    // Delete data
    public static function deleteUser($where, $limit) {

        // Delete data form DB
        $data = Database::db()
            ->table("users")
            ->where($where)
            ->limit($limit)
            ->delete();

        // Return data
        return $data;
    }

    // Update data
    public static function updateUser($params, $where, $limit) {

        // Generate limit clause
        $data = Database::db()
            ->table("users")
            ->fields($params)
            ->where($where)
            ->limit($limit)
            ->update();

        // Return data
        return $data;
    }
}