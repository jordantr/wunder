<?php
/**
 * Smarty plugin
 *
 * @package    Smarty
 * @subpackage PluginsModifierCompiler
 */

/**
 * Smarty custom format modifier plugin
 * Type:     modifier<br>
 * Name:     custom format<br>
 * Purpose:  convert number to human readable
 *
 * @author Jordan T
 *
 * @param array $params parameters
 *
 * @return number in human readable format
 */

function smarty_modifier_customformat($number)
{
	// If not numeric or already formated
	if(!is_numeric($number)){
		return $number;
	}

	// Format in millions/billions
    if ($number < 1000000) {
	    $format = number_format($number);
	} else if ($number < 1000000000) {
	    $format = number_format($number / 1000000, 2) . 'M';
	} else {
	    $format = number_format($number / 1000000000, 2) . 'B';
	}

	// Return data
	echo $format;
}