<?php

/**
 *
 * This file may not be redistributed in whole or significant part
 *
 * ------------------- THIS IS NOT FREE SOFTWARE -------------------
 *
 * Copyright 2021 All Rights Reserved
 * 
 * System class
 * 
 * @file        System.class.php
 * @category    core
 * @author		Jordan Trifonov
 *   	
 */
class System {

    // Check if post request
    public static function isPost() {

        // Check if post request
        if (strtolower($_SERVER['REQUEST_METHOD']) == "post") {
            return true;
        } else {
            return false;
        }
    }

    // Check if get request
    public static function isGet() {

        // Check if post request
        if (strtolower($_SERVER['REQUEST_METHOD']) == "get") {
            return true;
        } else {
            return false;
        }
    }

    // Check if ajax request
    public static function isAjax() {

        // Check if ajax request
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            return true;
        } else {
            return false;
        }
    }

    // Redirect member
    public static function redirect($url, $code = "") {

        // Chech if some headers are sent
        if (!headers_sent()) {

            // If there is a code set
            if (!empty($code)) {
                header('Location: ' . $url, true, $code);
            } else {
                header('Location: ' . $url);
            }

            // If the headers are sent - redirect via JavaScript
        } else {
            echo '<script type="text/javascript">';
            echo 'window.location.href="' . $url . '";';
            echo '</script>';
            echo '<noscript>';
            echo '<meta http-equiv="refresh" content="0;url=' . $url . '" />';
            echo '</noscript>';
        }

        // Terminate
        exit();
    }

}