<?php

/**
 *
 * This file may not be redistributed in whole or significant part
 *
 * ------------------- THIS IS NOT FREE SOFTWARE -------------------
 *
 * Copyright 2021 All Rights Reserved
 * 
 * Form validation class
 * 
 * @file        Form.class.php
 * @category    core
 * @author		Jordan Trifonov
 *   	
 */
class Form {

    // Errors
    private $errors = array();

    // Validate
    public function validate($fields) {

        // For each rule
        foreach ($fields AS $field) {

            // Check rules for the field
            foreach ($field['rules'] AS $rule_name => $rule_value) {

                // Check what the rule is
                switch ($rule_name) {

                    // Length
                    case "length":
                        $string_size = explode(",", $rule_value);
                        if (strlen($field['value']) < $string_size[0] OR strlen($field['value']) > $string_size[1]) {
                            $this->errors[$field['field']][] = (!empty($field['errors'][$rule_name])) ? $field['errors'][$rule_name] : "";
                        }
                        break;

                    // Alpha
                    case "alpha":
                        if (!ctype_alpha($field['value'])) {
                            $this->errors[$field['field']][] = (!empty($field['errors'][$rule_name])) ? $field['errors'][$rule_name] : "";
                        }
                        break;

                    // Numeric
                    case "numeric":
                        if (!is_numeric($field['value'])) {
                            $this->errors[$field['field']][] = (!empty($field['errors'][$rule_name])) ? $field['errors'][$rule_name] : "";
                        }
                        break;

                    // Alphanumeric
                    case "alphanumeric":
                        if (!ctype_alnum($field['value'])) {
                            $this->errors[$field['field']][] = (!empty($field['errors'][$rule_name])) ? $field['errors'][$rule_name] : "";
                        }
                        break;

                    // Integer
                    case "integer":
                        if (!is_int($field['value'])) {
                            $this->errors[$field['field']][] = (!empty($field['errors'][$rule_name])) ? $field['errors'][$rule_name] : "";
                        }
                        break;

                    // Float
                    case "float":
                        if (!is_float($field['value'])) {
                            $this->errors[$field['field']][] = (!empty($field['errors'][$rule_name])) ? $field['errors'][$rule_name] : "";
                        }
                        break;

                    // Date
                    case "date":

                        // Check syntax
                        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $field['value'])) {
                            $this->errors[$field['field']][] = (!empty($field['errors'][$rule_name])) ? $field['errors'][$rule_name] : "";

                            // Check for valid gregorian date
                        } else {
                            $parts = explode("-", $field['value']);
                            if (!checkdate($parts[1], $parts[2], $parts[0])) {
                                $this->errors[$field['field']][] = (!empty($field['errors'][$rule_name])) ? $field['errors'][$rule_name] : "";
                            }
                        }

                        break;

                    // Email
                    case "email":
                        if (!filter_var($field['value'], FILTER_VALIDATE_EMAIL)) {
                            $this->errors[$field['field']][] = (!empty($field['errors'][$rule_name])) ? $field['errors'][$rule_name] : "";
                        }
                        break;

                    // IP
                    case "ip":
                        if (!filter_var($field['value'], FILTER_VALIDATE_IP)) {
                            $this->errors[$field['field']][] = (!empty($field['errors'][$rule_name])) ? $field['errors'][$rule_name] : "";
                        }
                        break;

                    // Enum
                    case "enum":

                        // If the value matches one of the enum possible values
                        $found = 0;

                        // For each possible value
                        foreach ($rule_value AS $possible_value) {

                            // If it's found in the enum values
                            if ($field['value'] == $possible_value) {
                                $found = 1;
                            }
                        }

                        // If the value was not in the enum possible values
                        if ($found == 0) {
                            $this->errors[$field['field']][] = (!empty($field['errors'][$rule_name])) ? $field['errors'][$rule_name] : "";
                        }
                        break;
                }
            }
        }

        // If no errors
        if (empty($this->errors)) {
            return true;
        } else {
            return false;
        }
    }

    // Get error
    public function getError($field) {
        return !empty($this->errors[$field]) ? $this->errors[$field][0] : false;
    }

    // Get all errors for a field
    public function getErrors($field) {
        return !empty($this->errors[$field]) ? $this->errors[$field] : false;
    }

    // Get all errors
    public function getAllErrors() {

        // Combine errors in better array to assign it to Smarty
        if (!empty($this->errors)) {

            // For each field's errors
            foreach ($this->errors AS $field => $errors) {

                // For each error
                foreach ($errors AS $error) {
                    $return_errors[] = $error;
                }
            }

            // Return errors
            return $return_errors;

            // No errors found
        } else {
            return false;
        }
    }

    // Get all errors
    public function getFirstError()
    {

        // Combine errors in better array to assign it to Smarty
        if (!empty($this->errors)) {

            // Get first error
            $error = array_slice($this->errors, 0, 1);

            // Get error text
            $error_text = reset($error);
            $return_error['error'] = reset($error_text);

            // Get field name
            $return_error['error_field'] = key($error);

            // Return errors
            return $return_error;

            // No errors found
        } else {
            return false;
        }
    }
}