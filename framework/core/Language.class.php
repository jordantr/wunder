<?php

/**
 *
 * This file may not be redistributed in whole or significant part
 *
 * ------------------- THIS IS NOT FREE SOFTWARE -------------------
 *
 * Copyright 2021 All Rights Reserved
 * 
 * Language class
 * 
 * @file        Language.class.php
 * @category	core
 * @author		Jordan Trifonov
 *   	
 */
class Language {

    // Load language
    public static function load($lang_code) {

        // Get language files path
        $path = ROOT_PATH . "application/lang/" . strtolower($lang_code) . "/";

        // Create a handler for the directory
        $handler = opendir($path);

        // Keep going until all files in directory have been read
        while ($file = readdir($handler)) {

            // If the results is file
            if ($file != '.' && $file != '..' && $file != '.svn') {

                // Include the language file
                include_once($path . $file);
            }
        }

        // Close the handler
        closedir($handler);

        // Return language files data
        return $lang;
    }

}