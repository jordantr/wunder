<?php

/**
 *
 * This file may not be redistributed in whole or significant part
 *
 * ------------------- THIS IS NOT FREE SOFTWARE -------------------
 *
 * Copyright 2021 All Rights Reserved
 * 
 * Main controller class
 * 
 * @file        Controller.class.php
 * @category    core
 * @author		Jordan Trifonov
 *   	
 */
class Controller {

    // Set default controller path
    private $_defaultPath = "application/controllers/";
    // Default controller
    private $_defaultController = "Index";
    // Default action
    private $_defaultAction = "index";
    // Controller
    public $_controller;
    // Action
    public $_action;
    // Site configuration
    public $params;
    // Holder for url array, later used to get params from it
    public $url_array;

    // Check if file exists
    private function checkFile($file_path) {
        return file_exists($file_path);
    }

    // Check if method exists
    private function checkMethod($object, $method) {
        return method_exists($object, $method);
    }

    // Filter for controller and action
    private function filterAlphaNumeric($str = "") {
        return preg_replace('/[^a-z0-9]+/i', "", $str);
    }

    // Main controller and action management
    public function dispatch() {

        // Get current url
        $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
        $url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

        // Remove main url before searching for controller and action
        $url = str_replace($this->params['config']['site_url'], "", $url);

        // Split into array
        $url_array = explode("/", $url);
        $this->url_array = $url_array;

        // Set the controller
        $controller = (!empty($url_array[0])) ? $url_array[0] : "";
        $controller = $this->filterAlphaNumeric($controller);
        $this->_controller = (empty($controller)) ? $this->_defaultController : ucfirst(strtolower($controller));

        // Set the action
        $action = (!empty($url_array[1])) ? $url_array[1] : "";
        $action = $this->filterAlphaNumeric($action);
        $this->_action = (empty($action)) ? $this->_defaultAction : ucfirst(strtolower($action));

        // Form the controller path
        $controller_path = $this->_defaultPath . $this->_controller . 'Controller.php';

        // Check if the current controller file exists
        if (!$this->checkFile($controller_path)) {
            header("HTTP/1.0 404 Not Found");
            System::redirect("/error");
        }

        // Load the controller file
        require_once($controller_path);

        // Get controller class name
        $controller_class = $this->_controller . 'Controller';

        // Create controller object
        $controller = new $controller_class;

        // Check if init method exists and execute it if exists
        if ($this->checkMethod($controller, 'init')) {
            $controller->init();
        }

        // Check action - if no action then use the call method
        $action_name = $this->_action . "Action";

        if (!$this->checkMethod($controller_class, $action_name)) {
            $action_name = "_call";
        }

        // Run controller action
        $controller->$action_name();
    }

    // Get URL parameters
    public function getParam($variable, $default = NULL) {

        // Get current url
        $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

        // Remove main url before searching for controller and action
        $url = str_replace($this->params['config']['site_url'], "", $url);

        // Split into array
        $url_array = explode("/", $url);
        $this->url_array = $url_array;
        
        // Get the key of the param
        $key = array_search($variable, $this->url_array);

        // If the parameter is found get the value
        if (!empty($key)) {
            return $this->url_array[$key + 1];
        } else {
            return $default;
        }
    }

}