<?php

/**
 *
 * This file may not be redistributed in whole or significant part
 *
 * ------------------- THIS IS NOT FREE SOFTWARE -------------------
 *
 * Copyright 2021 All Rights Reserved
 * 
 * Registry class
 * 
 * @file		Registry.class.php
 * @category	core
 * @author		Jordan Trifonov
 *   	
 */
class Registry {

    // Storage
    public static $register = array();

    // Set value
    public static function set($name = "", $value) {
        self::$register[$name] = $value;
    }

    // Get value
    public static function get($name) {
        return self::$register[$name];
    }

    // Check value
    public static function isRegistered($name) {
        return array_key_exists($name, self::$register);
    }

}