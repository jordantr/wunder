<?php

/**
 *
 * This file may not be redistributed in whole or significant part
 *
 * ------------------- THIS IS NOT FREE SOFTWARE -------------------
 *
 * Copyright 2021 All Rights Reserved
 * 
 * Database class
 * 
 * @file        Database.class.php
 * @category    core
 * @author      Jordan Trifonov
 *      
 */
class Database {

    // PDO connection
    public $_connection;

    // Cache object
    public $cache;

    // Master Enable/Disable for cache
    public $enableCache = false;

    // PDO object from prepared and/or executed query
    public $query_object;

    // Debugger on or off
    public $debugger;

    // Queries counter
    public $debug_counter = 1;

    // Fields
    public $fields;

    // Main table
    public $table;

    // Joined tables
    public $joins;

    // Where clause
    public $where;

    // Order clause
    public $order;

    // Group
    public $group;

    // Having
    public $having;

    // Limit clause
    public $limit;

    // SQL to execute
    public $sql;
    
    // SQL params to use
    public $sql_params = array();

    // Connect to DB
    public function __construct($config) {

        // Check the existing connection
        if ($this->_connection) {
            return;
        }

        // Try to connect to the db
        try {
            $db = new PDO("mysql:host=" . $config['host'] . ";dbname=" . $config['database'], $config['username'], $config['password']);
        } catch (PDOException $e) {
            echo "MySQL Connection error: ".$e->getMessage();
            exit;
        }

        // Set the connection
        $this->_connection = $db;

        // Set encoding
        $this->_connection->query("SET NAMES '" . $config['charset'] . "'");

        // Check to start or not the debugger
        if(!empty($_SERVER['REMOTE_ADDR']) AND !empty($config['debugger_ips']) && in_array($_SERVER['REMOTE_ADDR'], $config['debugger_ips'])) {
            $this->debugger = true;
        } else {
            $this->debugger = false;
        }
        
        // Override by config settings
        if(isset($config['debugger']) && !$config['debugger']){
            $this->debugger = false;
        }
    }

    // Alias to get the object easier
    public static function db() {
        return Registry::get("db");
    }

    // Fields
    public function fields($fields, $prefix = '') {
        
        if(is_array($fields)){
            
            // Add custom prefix to the fields
            if(strlen($prefix) > 0){
                foreach($fields AS $fieldKey => $field){
                    
                    // Check for preset prefix
                    if(
                        substr($field, 0, strlen($prefix) + 1) == $prefix . '.' 
                        || strpos(strtolower($field), 'sum(') !== false
                        || strpos(strtolower($field), 'if(') !== false
                        || strpos(strtolower($field), 'count(') !== false
                        || strpos(strtolower($field), 'min(') !== false
                        || strpos(strtolower($field), 'max(') !== false
                        || strpos(strtolower($field), 'unix_timestamp(') !== false
                        || strpos(strtolower($field), 'str_to_date(') !== false
                        || strpos(strtolower($field), 'match(') !== false
                        || strpos(strtolower($field), 'md5(') !== false
                        
                        || strpos(strtolower($field), 'current_timestamp') !== false
                    ){
                        $fields[$fieldKey] = $field;
                    }else{
                        $fields[$fieldKey] = $prefix . '.' . $field;
                    }
                }
            }
            
            // Check if there are fields already set
            if(is_array($this->fields) && !empty($this->fields)){
                // Update/Replace fields or values
                foreach($fields AS $fieldKey => $field){
                    $this->fields[] = $field;
                }
            }else{
                if(strlen($this->fields) > 0){
                    $this->fields = array($this->fields);
                    
                    foreach($fields AS $fieldKey => $field){
                        $this->fields[] = $field;
                    }
                }else{
                    $this->fields = $fields;
                }  
            }
        }else{
            $this->fields = $fields;
        }

        // Return the object
        return $this;
    }

    // Group
    public function group($fields) {

        // Implode all fields
        $fields = implode(", ", $fields);

        // Add group by statement
        if(strlen($fields) > 0){
            $this->group = "GROUP BY " . $fields;
        }
        
        // Return the object
        return $this;
    }
    
    // Having
    public function having($condition) {
        
        $this->having = "HAVING " . $condition;
        
        // Return the object
        return $this;
    }

    // Get the main table
    public function table($table, $alias = "") {
        $this->table = $table;

        // If the alias is not empty - set it
        if (!empty($alias)) {
            $this->table .= " AS " . $alias;
        }

        // Return the object
        return $this;
    }

    // Left join statement
    public function leftJoin($table, $alias, $joinFields) {
        $this->joins[] = "LEFT JOIN " . $table . " AS " . $alias . " ON " . $joinFields;

        // Return the object
        return $this;
    }

    // Form the where clause of the query
    public function where($array, $glue = "AND", $extractConditions = false) {
        
        // If we have where conditions
        if (is_array($array) AND ! empty($array)) {

            // Form search array
            foreach ($array AS $k => $v) {
                
                // PDO named placeholders for queries are limited to [a-zA-Z0-9_], any other chars must be filtered
                $namedPlaceholder = preg_replace('/[^a-zA-Z0-9_]/', '_', $k);

                // Void
                if ($k == 'void' || substr($k, 0, 4) == 'void') {
                    $input[] = "(" . $v . ")";
                    continue;
                    
                // Match against
                } else if (strpos($v, "MATCH") !== false && strpos($v, "AGAINST") !== false) {
                    $input[] = $v;
                    continue;
                    
                // SQL functions and variables
                } else if (
                    strpos(strtolower($v), "current_timestamp")
                ) {
                    $input[] = $k . ' ' . $v;
                    continue;
                    
                // Like
                } else if (strpos($v, "%") !== false) {
                    $input[] = $k . " LIKE :where_" . $namedPlaceholder . "";

                // Different
                } else if (strpos($v, "!=") !== false) {
                    $input[] = $k . " != :where_" . $namedPlaceholder;

                // Lower / equal
                } else if (strpos($v, "<=") !== false) {
                    $input[] = $k . " <= :where_" . $namedPlaceholder;

                // Greater / equal
                } else if (strpos($v, ">=") !== false) {
                    $input[] = $k . " >= :where_" . $namedPlaceholder;

                // Lower
                } else if (strpos($v, "<") !== false) {
                    $input[] = $k . " < :where_" . $namedPlaceholder;

                // Greater
                } else if (strpos($v, ">") !== false) {
                    $input[] = $k . " > :where_" . $namedPlaceholder;

                // Dates
                } else if (strtolower(substr($k, 0, 4)) == 'date') {
                    $input[] = $k . " = '" . $v . "'";
                    continue;
                    
                // Between
                } else if (strpos($v, "BETWEEN") !== false) {
                    $input[] = $k . " " . $v;
                    continue;

                // In
                } else if (strpos($v, " IN ") !== false) {
                    $input[] = $k . " " . $v;
                    continue;

                // NULL
                } else if (strpos($v, "NULL") !== false) {
                    $input[] = $k . " " . $v;
                    
                    // Skip binding if we are dealing with NULL values
                    continue;

                // Equal
                } else {
                    $input[] = $k . " = :where_" . $namedPlaceholder;
                }
                
                // Extract values
                $v = str_replace(array("!=", "<=", ">=", "<", ">"), "", $v);
                $v = trim($v);
                $where['sql_params'][':where_'.$namedPlaceholder] = $v;
            }
            
            $conditions = implode(" " . $glue . " ", $input);
            
            // If we need to return generated where clause
            if($extractConditions){
                return $this->interpolateQuery($conditions, (isset($where) ? $where['sql_params'] : array()));
            }
            
            // Form search string
            $where['sql'] = " WHERE " . $conditions;

            // Merge to replace PDO parameters
            if(!empty($where['sql_params'])){
                $this->sql_params = array_merge($this->sql_params, $where['sql_params']);
            }

            // Return search string
            $this->where = $where;

            // If we don't have where conditions
        } else {
            $this->where['sql'] = "";
        }

        // Return the object
        return $this;
    }

    // Form the order clause of the query
    public function order($array) {

        // If we have order conditions
        if (!empty($array['by']) AND ! empty($array['type'])) {
            
            // Check for previous conditions
            if(isset($this->order) && strlen($this->order) > 0){
                $this->order .= ", " . $array['by'] . " " . $array['type'];
            }else{
                $this->order = "ORDER BY " . $array['by'] . " " . $array['type'];
            }
            
            // If we don't have order conditions
        } else {
            $this->order = "";
        }

        // Return the object
        return $this;
    }

    // Form the limit clause of the query
    public function limit($array) {

        // If we have limit conditions
        if (isset($array['from']) AND isset($array['count'])) {

            // If from is zeto - don't consider it
            if (empty($array['from'])) {
                $this->limit = "LIMIT " . $array['count'];
            } else {
                $this->limit = "LIMIT " . $array['from'] . "," . $array['count'];
            }

            // If we don't have limit conditions
        } else {
            $this->limit = "";
        }

        // Return the object
        return $this;
    }

    // Get the last insert id
    public function lastInsertId() {

        // Return the last insert id
        return $this->_connection->lastInsertId();
    }

    // Build query
    public function build($type) {

        // Check what type is the query
        switch($type){

            // Insert
            case "insert":

                // Form the params array
                foreach ($this->fields AS $k => $v) {
                    $keys[] = "`" . $k . "`";
                    $values_prepared[] = ":fields_" . $k;
                    $values[":fields_" . $k] = $v;
                }

                // Prepare SQL
                $this->sql = "INSERT INTO " . $this->table . " (" . implode(", ", $keys) . ") VALUES (" . implode(", ", $values_prepared) . ")";

                // Add params for PDO usage
                $this->sql_params = array_merge($this->sql_params, $values);
                break;

            // Update
            case "update":

                // Form the params array
                foreach ($this->fields AS $k => $v) {
                    $keys[] = "`" . $k . "`";
                    $values_prepared[] = "`" . $k . "` = :fields_" . $k;
                    $values[":fields_" . $k] = $v;
                }

                // Prepare SQL
                $this->sql = "UPDATE " . $this->table . " SET " . implode(", ", $values_prepared) . " " . $this->where['sql'] . " " . $this->limit;

                // Add params for PDO usage
                $this->sql_params = array_merge($this->sql_params, $values);
                break;

            // Delete
            case "delete":

                // Prepare SQL
                $this->sql = "DELETE FROM " . $this->table . " " . $this->where['sql'] . " " . $this->limit;
                break;

            // Select statements
            case "select":

                // Joins
                if (!empty($this->joins)) {
                    $joins = implode(" ", $this->joins);
                } else {
                    $joins = "";
                }

                // If executing manual statements it is possible to have missing sql key
                if(empty($this->where['sql'])){
                    $this->where['sql'] = "";
                }
                $this->sql = "SELECT " . (is_array($this->fields) ? implode(",", $this->fields) : $this->fields) . " FROM " . $this->table . " " . $joins . " " . $this->where['sql'] . " " . $this->group . " " . $this->having . " " . $this->order . " " . $this->limit;

                break;

        }
    }

    // Insert data
    public function insert() {

        // Build the query
        $this->build("insert");

        // Execute the query
        $result = $this->execute();

        // If the query is not executed properly
        if (!$result) {
            return false;
        } else {
            return $this->lastInsertId();
        }
    }

    // Update data
    public function update() {

        // Build the query
        $this->build("update");

        // Execute the query
        $result = $this->execute();

        // Check if the query is executed properly
        if (!$result) {
            return false;
        } else {
            return true;
        }
    }

    // Delete data
    public function delete() {

        // Build the query
        $this->build("delete");

        // Execute the query
        $result = $this->execute();

        // Check if the query is executed properly
        if (!$result) {
            return false;
        } else {
            return true;
        }
    }

    // Custom query execution
    public function query($explicitQuery) {

        // Set the query
        $this->sql = $explicitQuery;

        // Execute the query
        $this->execute();

        // Fetch data
        $result = $this->query_object->fetchAll(PDO::FETCH_ASSOC);

        // Check if the query is executed properly
        if (!$result) {
            return false;
        } else {
            return true;
        }
    }

    // Get all rows
    public function fetchAll($explicitQuery = '', $cacheParams = []) {
        
        // If we need to use custom query
        if(!empty($explicitQuery)){
            $this->sql = $explicitQuery;

        // If we need to use the default query builder
        } else {
        
            // Build the query
            $this->build("select");
        }
        
        // Master cache controls
        if($this->enableCache && !empty($cacheParams)){
            
            $queryHash = '';
            
            if(
                empty($explicitQuery) 
                && isset($cacheParams['cachePrefix']) && !empty($cacheParams['cachePrefix'])
            ){
                $queryHash = $cacheParams['cachePrefix'] . md5($this->interpolateQuery($this->sql, $this->sql_params));
                
                if($this->cache->exists($cacheParams, $queryHash)){
                    $result = json_decode($this->cache->get($cacheParams, $queryHash), true);
                    
                    // Clear object information
                    $this->unsetParams();
                    
                    return $result;
                }
            }
        }

        // Execute the query
        $this->execute();
        
        // Fetch data
        $result = $this->query_object->fetchAll(PDO::FETCH_ASSOC);
        
        // Master cache controls
        if($this->enableCache && !empty($cacheParams)){
            if(
                empty($explicitQuery) 
                && isset($cacheParams['cachePrefix']) && !empty($cacheParams['cachePrefix'])
            ){
                $this->cache->set($cacheParams, $queryHash, json_encode($result));
            }  
        }
        
        // Return fetched data
        return $result;
    }

    // Get row
    public function fetchRow($explicitQuery = '', $cacheParams = []) {

        // If we need to use custom query
        if(!empty($explicitQuery)){
            $this->sql = $explicitQuery;

        // If we need to use the default query builder
        } else {

            // Build the query
            $this->build("select");
        }
        
        // Master cache controls
        if($this->enableCache && !empty($cacheParams)){
            
            $queryHash = '';
            
            if(
                empty($explicitQuery) 
                && isset($cacheParams['cachePrefix']) && !empty($cacheParams['cachePrefix'])
            ){
                $queryHash = $cacheParams['cachePrefix'] . md5($this->interpolateQuery($this->sql, $this->sql_params));
                
                if($this->cache->exists($cacheParams, $queryHash)){
                    $result = json_decode($this->cache->get($cacheParams, $queryHash), true);
                    
                    // Clear object information
                    $this->unsetParams();
                    
                    return $result;
                }
            }
        }

        // Execute the query
        $this->execute();

        // Fetch data
        $result = $this->query_object->fetch(PDO::FETCH_ASSOC);
        
        // Master cache controls
        if($this->enableCache && !empty($cacheParams)){
            if(
                empty($explicitQuery) 
                && isset($cacheParams['cachePrefix']) && !empty($cacheParams['cachePrefix'])
            ){
                $this->cache->set($cacheParams, $queryHash, json_encode($result));
            }  
        }

        // Return fetched data
        return $result;
    }

    // Get one field
    public function fetchOne($explicitQuery = "") {

        // If we need to use custom query
        if(!empty($explicitQuery)){
            $this->sql = $explicitQuery;

        // If we need to use the default query builder
        } else {

            // Build the query
            $this->build("select");
        }
        
        // Master cache controls
        if($this->enableCache && !empty($cacheParams)){
            
            $queryHash = '';
            
            if(
                empty($explicitQuery) 
                && isset($cacheParams['cachePrefix']) && !empty($cacheParams['cachePrefix'])
            ){
                $queryHash = $cacheParams['cachePrefix'] . md5($this->interpolateQuery($this->sql, $this->sql_params));
                
                if($this->cache->exists($cacheParams, $queryHash)){
                    $result = $this->cache->get($cacheParams, $queryHash);
                    
                    // Clear object information
                    $this->unsetParams();
                    
                    return $result;
                }
            }
        }

        // Execute the query
        $this->execute();
        
        // Fetch data
        $result = $this->query_object->fetch(PDO::FETCH_NUM);
        
        // Master cache controls
        if($this->enableCache && !empty($cacheParams)){
            if(
                empty($explicitQuery) 
                && isset($cacheParams['cachePrefix']) && !empty($cacheParams['cachePrefix'])
            ){
                $this->cache->set($cacheParams, $queryHash, $result[0]);
            }  
        }

        // Return fetched data
        return $result[0];
    }

    // Prepare query for debugger
    public function interpolateQuery($query, $params = array()) {

        // Array for keys
        $keys = array();
        
        // Build a regular expression for each parameter
        foreach ($params as $key => &$value) {

            $keys[] = '/'.preg_quote($key).'/';
            
            if(
                !is_int($value)
                && !is_float($value)
                && !ctype_digit($value)
                && strpos($value, "NULL") === false
                && $value !== "''"
                && $value !== '""'
            ){
                $value = "'".$value."'";
            }
        }
        
        // Replace PDO statements with real values
        $query = preg_replace($keys, $params, $query, 1, $count);
        
        // Return data
        return $query;
    }

    // Execute query
    public function execute() {

        // Get start time
        $start = microtime(true);
        
        // Execure the query
        $this->query_object = $this->_connection->prepare($this->sql);
        
        // Try to execute
        try {

            // Activate PDO errors
            // $this->query_object->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
            
            // Get result if possible
            $result = $this->query_object->execute($this->sql_params);
            
            // Log bad query if result was false
            if($result === false){

                // Check for empty request URI
                if(empty($_SERVER['REQUEST_URI'])){
                    $_SERVER['REQUEST_URI'] = "";
                }

                // Log the error
                print_r($this->query_object->errorInfo()); 
            }

        // In case of error
        } catch (PDOException $e) {
            
            // Show the error to devs only
            if(!empty($config['developers_access']) && in_array($_SERVER['REMOTE_ADDR'], $config['developers_access']['debugger'])) {
                echo $e->getMessage();
            }
            exit;
        }

        // Get end time
        $end = microtime(true);

        // If the debugger is on - add the time and the query
        if ($this->debugger) {
            $this->debugger_data[] = array("time" => round($end - $start, 4), "query" => $this->interpolateQuery($this->sql, $this->sql_params));
        }
        
        $this->unsetParams();
        
        // Return the result
        return $result;
    }
    
    public function unsetParams() {
        
        // Unset params
        $this->fields = $this->table = $this->order = $this->group = $this->limit = $this->sql = "";
        $this->sql_params = $this->where = $this->joins = array();
    }

    // Show debugger
    public function showSQLDebugger() {

        // Check if the debugger is on
        if ($this->debugger) {

            // If we are using CLI
            if(php_sapi_name() === 'cli'){

                // Show headings
                echo "\nMySQL Debugger\n\n";
                echo "#   |  Time  | Query\n";
                echo "\n";
                $time = 0;
                if (!empty($this->debugger_data)) {
                    foreach ($this->debugger_data as $key => $value) {
                        $time += $value['time'];
                        echo str_pad($key, 3) ." | ".round($value['time'], 4) . " | ".$value['query']."\n";
                    }
                }
                echo "\nAll queries time: ".$time."s\n";

            // Web debugger
            } else {

                // Form the main table
                $return = '
                    <table border="1" cellpadding="2" cellspacing="2" width="100%" style="font-size: 12px; background-color: white; font-family: Arial, Helvetica, sans-serif;">
                        <tr>
                            <td align="center" colspan="3"><b>MySQL Debugger Results</b></td>
                        </tr>
                        <tr>
                            <td align="center" width="5%"><i>#</i></td>
                            <td align="left" width="75%"><i>MySQL Query</i></td>
                            <td align="center"><i>Time to get results</i></td>
                        </tr>
                ';

                // Store all queries time
                $time = 0;

                // Add queries data
                if (!empty($this->debugger_data)) {
                    foreach ($this->debugger_data as $key => $value) {
                        $return .= '
                        <tr>
                            <td align="center"><b>' . $key . '</b></td>
                            <td align="left" style="word-break: break-all;"><b>' . $value['query'] . '</b></td>
                            <td align="center"><b>' . $value['time'] . '</b></td>
                        </tr>
                    ';
                        $time += $value['time'];
                    }
                }

                $return .= '
                        <tr>
                            <td align="center" style="color: red"> - - </td>
                            <td align="left" style="color: red"><b>All Queries Time</b></td>
                            <td align="center" style="color: red"><b>' . $time . '</b></td>
                        </tr>
                    ';

                // Show the SQL debugger
                echo $return . "</table><br /><br />";
            }
        }
    }
}