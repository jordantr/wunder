<?php

/**
 *
 * This file may not be redistributed in whole or significant part
 *
 * ------------------- THIS IS NOT FREE SOFTWARE -------------------
 *
 * Copyright 2021 All Rights Reserved
 * 
 * Loader class
 * 
 * @file        Loader.class.php
 * @category    core
 * @author		Jordan Trifonov
 *   	
 */
class Loader {

    // Find and load class
    public static function loadClass($class) {

        // Posible files path
        $files_path = array(
            "framework/packages/" . $class . "/",
            "application/models/"
        );

        // Check all paths
        foreach ($files_path AS $path) {

            // Form the class file
            $file_path = ROOT_PATH . $path . $class . ".php";

            // Check if the class file exists
            if (is_file($file_path)) {

                // If the class or the interface are not there return
                if (class_exists($class, false) || interface_exists($class, false)) {
                    return;
                }

                // If everything is ok include the class file
                return include_once $file_path;
            }
        }
    }
}