<?php

/**
 * NPS 1.0
 *    
 * This file may not be redistributed in whole or significant part.
 * ------------------- NPS IS NOT FREE SOFTWARE -------------------
 * 
 * Copyright 2015 All Rights Reserved
 * 
 * Index - Bootstrap file of the MVC
 * 
 * @file			index.php
 * @category		bootstrap
 * @author		Jordan Trifonov
 *   	
 */

// Define root path to load the files later
define('ROOT_PATH', dirname(__FILE__) . "/");

// Load config and language files
require_once(ROOT_PATH . "application/config.php");

// Load main framework classes
require_once(ROOT_PATH . "framework/core/Controller.class.php");
require_once(ROOT_PATH . "framework/core/Registry.class.php");
require_once ROOT_PATH . "framework/core/Database.class.php";
require_once(ROOT_PATH . "framework/core/Loader.class.php");
require_once(ROOT_PATH . "framework/core/Language.class.php");
require_once(ROOT_PATH . "framework/core/Form.class.php");
require_once(ROOT_PATH . "framework/core/System.class.php");
require_once(ROOT_PATH . "framework/smarty/Smarty.class.php");

// Initialize smarty object
$smarty = new Smarty();
$smarty->template_dir = ROOT_PATH . 'application/views/';
$smarty->compile_dir = ROOT_PATH . 'temp/views_c/';
$smarty->config_dir = ROOT_PATH . 'temp/views_c/';
$smarty->cache_dir = ROOT_PATH . 'temp/cache/';
$smarty->force_compile = true;
$smarty->compile_check = false;
$smarty->debugging = false;
$smarty->caching = true;
$smarty->cache_lifetime = 3600;

// Create language instance
$lang = Language::load("en");

// Create DB instance
$db = new Database($config_db);

// Register configs
Registry::set("config", $config);
Registry::set("smarty", $smarty);
Registry::set("lang", $lang);
Registry::set("db", $db);

// Assign variables
$smarty->assign("lang", $lang);
$smarty->assign("config", $config);

// Start the controllers management
$controller = new Controller();
$controller->params = array(
    "config" => $config,
);
$controller->dispatch();