### How to setup the project ###

1. Change configs in application/config.php - site host and DB settings will be enough
2. Run /migrate.php in browser or console to create DB tables



### Describe possible performance optimizations for your Code ###

1. Database
- Better types and lengths of fields - what I made now is just for test purposes. In reality, I would consider how the application will be used to consider fields.
Example: Now for IBAN I'm adding VARCHAR(32) which is not good. But I'm doing it to handle whatever is added in tests up to 32 symbols. In reality, if we know where application will be used, I would limit the field - here is a list with IBANs max lengths by country:
https://www.iban.com/structure

2. Code
- I would use some ready packages and SDKs to fit into standards

3. Testing code before releasing
- I would make some unit tests to see how is the code performing
- I would greatly appreciate if we have some QAs or Selenium tests or whatever just to verify business logic works good



### Which things could be done better, than you’ve done it ? ###

1. Better error handling and reporting - Telegram or any other service
2. API calls can be collected and called in the background, timeout should be considered and retry system too since information is sensitive and needed in 100% of cases, so there should be no missing data in case of servers/network issues
3. Validations
4. Captcha - Google v3 invisible, just to stop bots
5. Protections - XSS, CSRF and etc, also 1 signup per 1 user
and many, many more things can be added, but they are more for real time project

In terms of code - I might really switch to Laravel/Yii2 if product should be supported/extended by another dev, current custom MVC solution is not providing docs and will be harder to review for a new developer
In terms of DB I think I did good with what MySQL provides at current stage



### Other information ###

Bootstrap and jQuery were used - I really don't like native elements and how they look in 21st century. jQuery is used for AJAX calls and showing success/error messages, code for which is in /htdocs/js/main.js file
I also used custom MVC - I was thinking to go with Yii2 or Laravel, but I decided to show you some code that I wrote and is working on live projects now (huge) - simple DB class and etc. are making it fast because it's highly customizable and no ORM is consuming resources
No PHP code from any source is used in the project - I wrote all the files/logic.
Users model contains deleteUser() model just to show the full CRUD functionality of models - not used in the demo task
Forms are using AJAX to pass data to backend, this is the best kind of data transfer so far so no page reloads or anything which takes time
I added validations with at least basic length to keep forms having errors and success messages to make the demo better