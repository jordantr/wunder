// Submit button for each step
$("#submit_button").on("click", function(){

	// Make post ajax request to save the data
	$.post( "/index/save/", $("#signup_form").serialize(), function(data) {

		// Parse response from server
		data = $.parseJSON(data);

		// If error
		if(data.error){

			// Show error message
			$("#message_error").html(data.error);
			$("#message_error").show();

		// If success
		} else {

			// Show success message
			$("#message_error").hide();
			$("#message_success").html("Loading, please wait...");
			$("#message_success").show();

			// Redirect to next step
			window.location.href = data.redirect_url;
		}
	});
});